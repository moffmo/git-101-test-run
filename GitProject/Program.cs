﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GitProject.Classes;

namespace GitProject
{
    class Program
    {
        static void Main(string[] args)
        {
            var gitProjectManager = new GitProjectManager(new MainClass(), 
                                                            new SecondClass(),
                                                            new ThirdClass(),
                                                            new FifthClass());

            while (Console.ReadLine() != "exit")
            {
                gitProjectManager.ShowMessages();
            }
        }
    }
}
