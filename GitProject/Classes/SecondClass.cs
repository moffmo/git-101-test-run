﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitProject.Classes
{
    public class SecondClass : IMessage
    {
        public string GetMessage()
        {
            return "This is the second class";
        }
    }
}
